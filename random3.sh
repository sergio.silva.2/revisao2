#!/bin/bash

# Verificar se foram fornecidos cinco parâmetros
if [ $# -ne 5 ]; then
    echo "Uso: $0 <arquivo1> <arquivo2> <arquivo3> <arquivo4> <arquivo5>"
    exit 1
fi

# Inicializar contadores
existentes=0
inexistentes=0

# Loop para verificar a existência dos arquivos
for arquivo in "$@"; do
    if [ -e "$arquivo" ]; then
        existentes=$((existentes + 1))
    else
        inexistentes=$((inexistentes + 1))
    fi
done

# Imprimir resultados
echo "Quantidade de arquivos existentes: $existentes"
echo "Quantidade de arquivos inexistentes: $inexistentes"
