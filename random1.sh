#!/bin/bash

# Função para gerar um número aleatório entre 2 e 13
gerar_numero_aleatorio() {
    echo $((RANDOM % 12 + 2))
}

# Loop para criar 3 arquivos vazios com nomes aleatórios
for i in {1..3}; do
    numero=$(gerar_numero_aleatorio)
    nome_arquivo="${numero}.txt"
    touch "$nome_arquivo"
    echo "Arquivo $nome_arquivo criado."
done
