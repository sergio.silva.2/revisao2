#!/bin/bash

# Função para gerar um número aleatório entre 2 e 13
gerar_numero_aleatorio() {
    echo $((RANDOM % 12 + 2))
}

# Função para exibir mensagem de erro em vermelho
exibir_mensagem_erro() {
    echo -e "\e[91mErro: $1\e[0m"
}

# Loop para criar 3 arquivos vazios com nomes aleatórios
for i in {1..3}; do
    numero=$(gerar_numero_aleatorio)
    nome_arquivo="${numero}.txt"

    # Verificar se o arquivo já existe
    if [ -e "$nome_arquivo" ]; then
        exibir_mensagem_erro "O arquivo $nome_arquivo já existe. Encerrando a execução."
        exit 1
    fi

    # Se o arquivo não existe, criá-lo
    touch "$nome_arquivo"
    echo "Arquivo $nome_arquivo criado."
done

echo "Processo concluído. Três arquivos foram criados no diretório atual."
